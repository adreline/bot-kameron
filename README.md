### How kam kameron became sentient boulder
###### Overview
Actually talking pebbles are rare finds, there exists only 3 species of sentient rocks on the planet.
Highest intelligence variation bares the latin name **_bouldera medita_**.
Kameron is a **exemplatory** specimen of this kind. Responds quickly and without stuttering.
Contrary to popular opinion on these beings, they do not belong to animal kingdom.
Studies show that sentient rocks are form of fungi who developed sentient hive mind to respond with clever jokes as a form of self defense.
Kameron does not require any form of care ( it is **_literally_** just a pebble ) and is ready all day and all night for conversations and more !

###### Stuff to talk about with kameron
just type in discord chat
```
kameron help
//or for information about specific command
kameron help <command name>
```
###### Do you want to teach kameron new things ?
No need to modify existing code ( *ok i lie but adding module is a breeze* )
Create new module file somefile.js and try to export only one function ( *it will be run in main kameron.js file* )
example module:
```
exports.decide = function(){
  //some code
  return result;
}
```
remember to use callbacks if dealing with asynchronus tasks ( *like database connections* ) :
```
exports.logMessage = function (callback){
  //some asynchronus task
  callback(result);
}
```
###### Do you want kameron to live in your own PC ?
you will need:

1. node.js
    - Discord.js
    - request
    - mysql
    - shell.js
2. mysql
3. Linux distribution ( *or anything that can run cron and bash commands* )
